//
//  BitbucketNetworkManager.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 09/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit

struct BitbucketNetworkManager {
    private let networkService = NetworkService<BitbucketEndPoint>()
    
    func getRepositories(completion: @escaping (Result<[ProfileBitbucket]>) -> ()) {
        networkService.request(endPoint: .repositories) { (data, response, error) in
            if let error = error {
                completion(Result.error(error))
            }
            if let data = data {
                do {
                    let values = try JSONDecoder().decode(Values.self, from: data)
                    completion(Result.success(values.values))
                } catch {
                    completion(Result.error(error))
                }
            }
        }
    }
    
    func getAvatar(urlString: String, completion: @escaping (Result<UIImage?>) -> ()) {
        networkService.request(endPoint: .avatar(urlString)) { (data, response, error) in
            if let error = error {
                completion(Result.error(error))
            }
            if let data = data {
                let image = UIImage(data: data)
                completion(Result.success(image))
            }
        }
    }
}
