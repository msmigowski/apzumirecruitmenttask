//
//  BitbucketEndPoint.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 09/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation

enum BitbucketEndPoint {
    case repositories
    case avatar(String)
}

extension BitbucketEndPoint: HTTPEndPoint {
    var baseUrl: URL {
        var url: String
        switch self {
        case .avatar(let urlString):
            url = urlString
        default:
            url = "https://api.bitbucket.org"
        }
        guard let baseUrl = URL(string: url) else {
            fatalError("Wrong url")
        }
        return baseUrl
    }
    
    var path: String {
        switch self {
        case .repositories:
            return "/2.0/repositories"
        default:
            return ""
        }
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
        return .requestWithParameters(urlParameters: [URLQueryItem(name: "fields", value: "values.name,values.owner,values.description") ])
    }
    
    var cache: HTTPCache {
        switch self {
        case .repositories:
            return .reload
        case .avatar:
            return .return
        }
    }
    
    
}
