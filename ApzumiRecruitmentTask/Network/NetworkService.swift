//
//  NetworkService.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation

typealias NetworkCompletion = (Data?, URLResponse?, Error?) -> ()

private enum NetworkError: String, Error {
    case urlQueryBuilding
    case requestBuilding
    case emptyUrl
}

class NetworkService<T: HTTPEndPoint> {
    private var task: URLSessionTask?
    
    func requestWithoutParameters(endPoint: T, completion: @escaping NetworkCompletion) {
        let session = URLSession.shared
        
        task = session.dataTask(with: endPoint.baseUrl, completionHandler: completion)
        task?.resume()
    }
    
    func request(endPoint: T, completion: @escaping NetworkCompletion) {
        let session = URLSession.shared
        
        do {
            let request = try buildRequest(endPoint)
            task = session.dataTask(with: request, completionHandler: completion)
            task?.resume()
        } catch {
            completion(nil, nil, error)
        }
    }
    
    func cancel() {
        task?.cancel()
    }
    
    private func buildRequest(_ endPoint: T) throws -> URLRequest {
        var request = URLRequest(url: endPoint.baseUrl.appendingPathComponent(endPoint.path))
        request.httpMethod = endPoint.method.rawValue
        request.cachePolicy = getCachePolicy(endPoint.cache)
        request.timeoutInterval = 30.0 // Arbitrary value, could be changed
        
        do {
            switch endPoint.task {
            case .request:
                break
            case .requestWithParameters(let urlParameters):
                try buildUrlQuery(request: &request, urlParameters: urlParameters)
            }
            return request
        } catch {
            throw error
        }
    }
    
    private func getCachePolicy(_ policy: HTTPCache) -> URLRequest.CachePolicy {
        switch policy {
        case .reload:
            return URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        case .return:
            return URLRequest.CachePolicy.returnCacheDataElseLoad
        }
    }
    
    private func buildUrlQuery(request: inout URLRequest, urlParameters: [URLQueryItem]) throws {
        guard let url = request.url else { throw NetworkError.emptyUrl }
        
        if var components = URLComponents(url: url, resolvingAgainstBaseURL: false) {
            components.queryItems = urlParameters
            request.url = components.url
        }
    }
}
