//
//  GithubNetworkManager.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit

struct GithubNetworkManager {
    private let networkService = NetworkService<GithubEndPoint>()
    
    func getRepositories(completion: @escaping (Result<[ProfileGithub]>) -> () ) {
        networkService.request(endPoint: .repositories) { (data, response, error) in
            if let error = error {
                completion(Result.error(error))
            }
            if let data = data {
                do {
                    let profiles = try JSONDecoder().decode([ProfileGithub].self, from: data)
                    completion(Result.success(profiles))
                } catch {
                    completion(Result.error(error))
                }
            }
        }
    }
    
    func getAvatar(urlString: String, completion: @escaping (Result<UIImage?>) -> ()) {
        networkService.requestWithoutParameters(endPoint: .avatar(url: urlString)) { (data, response, error) in
            if let error = error {
                completion(Result.error(error))
            }
            if let data = data {
                let image = UIImage(data: data)
                completion(Result.success(image))
            }
        }
    }
}
