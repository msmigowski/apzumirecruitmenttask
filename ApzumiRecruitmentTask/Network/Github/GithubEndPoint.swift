//
//  GithubEndPoint.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation

enum GithubEndPoint {
    case repositories
    case avatar(url: String)
}

extension GithubEndPoint: HTTPEndPoint {
    var baseUrl: URL {
        var url: String
        switch self {
        case .avatar(let urlString):
            url = urlString
        default:
            url = "https://api.github.com"
        }
        guard let baseUrl = URL(string: url) else {
            fatalError("Could not cerate url from string")
        }
        return baseUrl
    }
    
    var path: String {
        switch self {
        case .repositories:
            return "repositories"
        case .avatar:
            return ""
        }
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
        return .request
    }
    
    var cache: HTTPCache {
        switch self {
        case .avatar:
            return .return
        case .repositories:
            return .reload
        }
    }
}
