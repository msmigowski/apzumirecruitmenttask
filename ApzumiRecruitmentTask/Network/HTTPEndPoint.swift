//
//  HTTPEndPoint.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation

typealias HTTPHeaders = [String: String]
typealias Parameters = [String: String]

enum HTTPMethod: String {
    case get, post, put, delete, patch
}

enum HTTPTask {
    case request
    case requestWithParameters(urlParameters: [URLQueryItem])
}

enum HTTPCache {
    case reload
    case `return`
}

protocol HTTPEndPoint {
    var baseUrl: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var task: HTTPTask { get }
    var cache: HTTPCache { get }
}
