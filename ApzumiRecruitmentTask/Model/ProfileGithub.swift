//
//  ProfileGithub.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation

struct ProfileGithub: Profile {
    private(set) var date: Date = Date()
    private(set) var repositoryType: ProfileType = .github
    private(set) var username: String
    private(set) var repositoryName: String
    private(set) var avatarUrl: String
    private(set) var description: String?
}

extension ProfileGithub: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case repositoryName = "name"
        case description
        case owner
    }
    
    enum OwnerCodingKeys: String, CodingKey {
        case login
        case avatarUrl = "avatar_url"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        repositoryName = try container.decode(String.self, forKey: .repositoryName)
        description = try container.decode(String?.self, forKey: .description)
        
        let nestedOwner = try container.nestedContainer(keyedBy: OwnerCodingKeys.self, forKey: .owner)
        avatarUrl = try nestedOwner.decode(String.self, forKey: .avatarUrl)
        username = try nestedOwner.decode(String.self, forKey: .login)
    }
    
}
