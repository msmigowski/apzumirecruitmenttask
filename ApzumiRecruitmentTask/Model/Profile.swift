//
//  Profile.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 09/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation

enum ProfileType {
    case bitbucket
    case github
}

protocol Profile {
    var username: String { get }
    var repositoryName: String { get }
    var avatarUrl: String { get }
    var description: String? { get }
    var repositoryType: ProfileType { get }
    var date: Date { get }
}
