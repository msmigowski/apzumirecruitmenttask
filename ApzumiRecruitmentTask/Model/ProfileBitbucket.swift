//
//  ProfileBitbucket.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 09/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation

struct Values: Decodable {
    var values: [ProfileBitbucket]
}

struct ProfileBitbucket: Profile {
    private(set) var date: Date = Date()
    private(set) var repositoryType: ProfileType = .bitbucket
    private(set) var avatarUrl: String
    private(set) var description: String?
    private(set) var username: String
    private(set) var repositoryName: String
}

extension ProfileBitbucket: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case owner
        case repositoryName = "name"
        case description
    }
    
    enum OwnerCodingKeys: String, CodingKey {
        case username
        case links
    }
    
    enum LinksCodingKeys: String, CodingKey {
        case avatar
    }
    
    enum AvatarCodingKeys: String, CodingKey {
        case href
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        description = try container.decode(String?.self, forKey: .description)
        repositoryName = try container.decode(String.self, forKey: .repositoryName)
        
        let nestedOwner = try container.nestedContainer(keyedBy: OwnerCodingKeys.self, forKey: .owner)
        username = try nestedOwner.decode(String.self, forKey: .username)
        
        let nestedLinks = try nestedOwner.nestedContainer(keyedBy: LinksCodingKeys.self, forKey: .links)
        let nestedAvatar = try nestedLinks.nestedContainer(keyedBy: AvatarCodingKeys.self, forKey: .avatar)
        avatarUrl = try nestedAvatar.decode(String.self, forKey: .href)
    }
    
}
