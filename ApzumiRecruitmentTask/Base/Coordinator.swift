//
//  File.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit

enum CoordinatorPresentation {
    case window(UIWindow), present(UIViewController), push(UINavigationController)
}

protocol Coordinator {
    var presentation: CoordinatorPresentation { get }
    
    init(with presentation: CoordinatorPresentation)
    
    func start()
}

extension Coordinator {
    func present(controller: UIViewController) {
        switch presentation {
        case .present:
            break
        case .window(let window):
            // Here should be some code that replace current visible window
            window.rootViewController = controller
            window.makeKeyAndVisible()
        case .push:
            break
        }
    }
}
