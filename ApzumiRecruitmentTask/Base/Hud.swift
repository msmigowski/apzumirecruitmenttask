//
//  Hud.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 09/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit

protocol HUD {
    func showHud()
    func hideHud()
}

extension HUD where Self: ViewController {
    func showHud() {
        let hudView = HudView.loadFromNib()
        hudView.frame = self.view.bounds
        hudView.isActive = true
        self.view.addSubview(hudView)
    }
    
    func hideHud() {
        let hudView = self.findHud()
        hudView?.isActive = false
        hudView?.removeFromSuperview()
    }
    
    private func findHud() -> HudView? {
        return self.view.subviews
                        .filter { $0 is HudView }
                        .first as? HudView
    }
}
