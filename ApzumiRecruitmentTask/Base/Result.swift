//
//  Result.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case error(Error)
}
