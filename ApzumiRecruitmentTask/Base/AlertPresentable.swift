//
//  AlertPresentable.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 09/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit

protocol AlertPresentable {
    func presentDefaultAlert(_ message: String?)
}

extension AlertPresentable where Self: ViewController {
    func presentDefaultAlert(_ message: String?) {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
