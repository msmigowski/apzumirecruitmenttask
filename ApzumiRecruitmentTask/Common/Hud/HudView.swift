//
//  HudView.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 09/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit

class HudView: UIView, NibLoadable {
    
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!

}

// MARK: Activity indicator
extension HudView {
    var isActive: Bool {
        get {
            return activityIndicator.isAnimating
        }
        set {
            if newValue {
                activityIndicator.startAnimating()
                self.isHidden = false
            } else {
                activityIndicator.stopAnimating()
                self.isHidden = true
            }
        }
    }
}
