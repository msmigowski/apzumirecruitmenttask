//
//  AppCoordinator.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation

class AppCoordinator: Coordinator {
    var presentation: CoordinatorPresentation
    var childCoordinator: Coordinator?
    
    required init(with presentation: CoordinatorPresentation) {
        self.presentation = presentation
    }
    
    func start() {
        let mainCoordinator = MainCoordinator(with: presentation)
        mainCoordinator.start()
        childCoordinator = mainCoordinator
    }
    
    
}
