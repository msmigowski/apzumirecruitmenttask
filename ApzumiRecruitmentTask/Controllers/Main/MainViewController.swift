//
//  MainViewController.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit
import RxSwift

final class MainViewController: ViewController, HUD, AlertPresentable {
    
    var viewModel: MainViewModel!
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var sortButtonItem: UIBarButtonItem!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareView()
        viewModel.input.controller.viewDidLoad.onNext(())
    }
    
    func configure(with viewModel: MainViewModel) {
        self.viewModel = viewModel
        
        prepareRxObservers()
        viewModel.input.controller.viewDidConfigured.onNext(())
    }
}

// MARK: Preparation
extension MainViewController {
    private func prepareView() {
        tableView.register(UINib(nibName: "MainTableViewCell", bundle: nil) , forCellReuseIdentifier: "MainTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 76
        tableView.rowHeight = UITableView.automaticDimension
        
        sortButtonItem.title = "Sort"
    }
    
    private func prepareRxObservers() {
        viewModel.output.controller.reloadTableView.subscribe(onNext: { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }).disposed(by: disposeBag)
        
        viewModel.output.controller.hudToggle.subscribe(onNext: { [weak self] toggle in
            toggle ? self?.showHud() : self?.hideHud()
        }).disposed(by: disposeBag)
        
        viewModel.output.controller.showAlert.subscribe(onNext: { [weak self] message in
            self?.presentDefaultAlert(message)
        }).disposed(by: disposeBag)
    }
}

// MARK: Actions
extension MainViewController {
    @IBAction fileprivate func sortAction(_ sender: UIBarButtonItem) {
        // TODO: (MŚ) Kind of 'hacky' solution, especialy using strings in this case. Must be changed
        let sorted: Bool
        if sender.title == "Sort" {
            sender.title = "Unsort"
            sorted = true
        } else {
            sender.title = "Sort"
            sorted = false
        }
        viewModel.input.controller.didSortPressed.onNext(sorted)
    }
}

// MARK: UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellModel = viewModel.cellModelForRowAtIndexPath(indexPath) else {
            fatalError("Cannot init view model")
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath) as? MainTableViewCell else {
            fatalError("Wrong table view cell identifier")
        }
        cell.configure(with: cellModel)
        return cell
    }
}

// MARK: UITableViewDelegate
extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        viewModel.input.controller.didSelectRow.onNext(indexPath)
    }
}
