//
//  MainViewModel.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation
import RxSwift

final class MainViewModel: ViewModel {
    var input = Input()
    var output = Output()
    
    private var profiles = Variable<[Profile]>( [Profile]() )
    private let githubNetworkManager = GithubNetworkManager()
    private let bitbucketNetworkManager = BitbucketNetworkManager()
    private let dispatchGroup = DispatchGroup()
    
    override init() {
        super.init()
        
        prepareViewModel()
    }
}

// MARK: Preparation
extension MainViewModel {
    private func prepareViewModel() {
        prepareInput()
        prepareOutpur()
    }
    
    private func prepareInput() {
        input.controller.viewDidLoad.subscribe(onNext: { [weak self] in
            self?.output.controller.hudToggle.onNext(true)
            
            self?.dispatchGroup.enter()
            self?.downloadGithubRepositories()
            self?.dispatchGroup.enter()
            self?.downloadBitbucketRepositories()
            
            self?.dispatchGroup.notify(queue: DispatchQueue.main, execute: { [weak self] in
                self?.output.controller.hudToggle.onNext(false)
            })
        }).disposed(by: disposeBag)
        
        input.controller.didSelectRow.subscribe(onNext: { [weak self] indexPath in
            guard let profile = self?.profiles.value[indexPath.row] else { return }
            self?.output.coordinator.presentDetailsViewController.onNext(profile)
        }).disposed(by: disposeBag)
        
        input.controller.didSortPressed.subscribe(onNext: { [weak self] sorted in
            if sorted {
                self?.profiles.value.sort(by: { $0.repositoryName.lowercased() < $1.repositoryName.lowercased() })
            } else {
                self?.profiles.value.sort(by: { $0.date < $1.date })
            }
        }).disposed(by: disposeBag)
    }
    
    private func prepareOutpur() {
        profiles.asObservable().subscribe(onNext: { [weak self] profiles in
            self?.output.controller.reloadTableView.onNext(())
        }).disposed(by: disposeBag)
    }
}

// MARK: Networking
extension MainViewModel {
    private func downloadGithubRepositories() {
        githubNetworkManager.getRepositories { [weak self] (result) in
            self?.manageResult(result)
        }
    }
    
    private func downloadBitbucketRepositories() {
        bitbucketNetworkManager.getRepositories { [weak self] (result) in
            self?.manageResult(result)
        }
    }
    
    private func manageResult<T: Profile>(_ result: Result<[T]>) {
        defer { dispatchGroup.leave() }
        switch result {
        case .success(let profiles):
            self.profiles.value.append(contentsOf: profiles)
        case .error(let error):
            output.controller.showAlert.onNext(error.localizedDescription)
        }
    }
}

// MARK: UITableViewDataSource methods
extension MainViewModel {
    func numberOfRowsInSection(_ section: Int) -> Int {
        return profiles.value.count
    }
    
    func cellModelForRowAtIndexPath(_ indexPath: IndexPath) -> MainTableCellViewModel? {
        let profile = profiles.value[indexPath.row]
        return MainTableCellViewModel(with: profile)
    }
}

// MARK: Input/Output
extension MainViewModel {
    struct Input {
        var controller = ControllerInput()
        
        struct ControllerInput {
            var viewDidLoad = PublishSubject<Void>()
            var viewDidConfigured = PublishSubject<Void>()
            var didSelectRow = PublishSubject<IndexPath>()
            var didSortPressed = PublishSubject<Bool>()
        }
    }
    
    struct Output {
        var controller = ControllerOutput()
        var coordinator = CoordinatorOutput()
        
        struct CoordinatorOutput {
            let presentDetailsViewController = PublishSubject<Profile>()
        }
        
        struct ControllerOutput {
            let reloadTableView = PublishSubject<Void>()
            let hudToggle = PublishSubject<Bool>()
            let showAlert = PublishSubject<String?>()
        }
    }
}
