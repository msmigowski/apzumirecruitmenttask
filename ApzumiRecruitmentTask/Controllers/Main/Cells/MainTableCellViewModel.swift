//
//  MainTableCellModel.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import Foundation
import RxSwift

final class MainTableCellViewModel: ViewModel {
    let output = Output()
    
    private let profile: Profile
    private let githubNetworkManager = GithubNetworkManager()
    private let bitbucketNetworkManager = BitbucketNetworkManager()
    
    init(with profile: Profile) {
        self.profile = profile
        
        super.init()
        
        prepareViewModel()
    }
}

// MARK: Preparation
extension MainTableCellViewModel {
    private func prepareViewModel() {
        output.cell.repositoryName.onNext(profile.repositoryName)
        output.cell.userName.onNext(profile.username)
        
        if case .bitbucket = profile.repositoryType {
            output.cell.backgroundColor.onNext(.orange)
        }
        
        downloadAvatar()
    }
}

// MARK: Networking
extension MainTableCellViewModel {
    private func downloadAvatar() {
        switch profile.repositoryType {
        case .github:
            downloadGithubAvatar()
        case .bitbucket:
            downloadBitbucketAvatar()
        }
    }
    
    private func downloadGithubAvatar() {
        githubNetworkManager.getAvatar(urlString: profile.avatarUrl) { [weak self] result in
            switch result {
            case .success(let image):
                self?.output.cell.avatarImage.onNext(image)
            case .error(let error):
                print(error) // TODO: (MŚ) Make error handling by controllers
            }
        }
    }
    
    private func downloadBitbucketAvatar() {
        bitbucketNetworkManager.getAvatar(urlString: profile.avatarUrl) { [weak self] result in
            switch result {
            case .success(let image):
                self?.output.cell.avatarImage.onNext(image)
            case .error(let error):
                print(error)
            }
        }
    }
}

// MARK: Input/Output
extension MainTableCellViewModel {
    struct Output {
        let cell = CellOutput()
        
        struct CellOutput {
            let repositoryName = BehaviorSubject<String?>(value: nil)
            let userName = BehaviorSubject<String?>(value: nil)
            let avatarImage = BehaviorSubject<UIImage?>(value: nil)
            let backgroundColor = BehaviorSubject<UIColor>(value: .white)
        }
    }
}
