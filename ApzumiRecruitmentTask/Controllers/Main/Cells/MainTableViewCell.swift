//
//  MainTableViewCell.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class MainTableViewCell: TableViewCell {

    var viewModel: MainTableCellViewModel!
    
    @IBOutlet fileprivate weak var avatarImageView: UIImageView!
    @IBOutlet fileprivate weak var repositoryNameLabel: UILabel!
    @IBOutlet fileprivate weak var userNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with viewModel: MainTableCellViewModel) {
        self.viewModel = viewModel
        
        prepareCell()
    }
}

// MARK: Preparation
extension MainTableViewCell {
    private func prepareCell() {
        viewModel.output.cell.repositoryName.bind(to: repositoryNameLabel.rx.text).disposed(by: disposeBag)
        viewModel.output.cell.userName.bind(to: userNameLabel.rx.text).disposed(by: disposeBag)
        viewModel.output.cell.avatarImage.bind(to: avatarImageView.rx.image).disposed(by: disposeBag)
        viewModel.output.cell.backgroundColor.subscribe(onNext: { [weak self] color in
            self?.contentView.backgroundColor = color
        }).disposed(by: disposeBag)
    }
}
