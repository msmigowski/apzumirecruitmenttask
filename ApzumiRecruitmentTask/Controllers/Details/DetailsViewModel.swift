//
//  DetailsViewModel.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit
import RxSwift

class DetailsViewModel: ViewModel {
    let output = Output()
    let input = Input()
    
    private let profile: Profile
    private let githubNetworkManager = GithubNetworkManager()
    
    init(with profile: Profile) {
        self.profile = profile
        
        super.init()
        prepareViewModel()
    }
}

// MARK: Preparation
extension DetailsViewModel {
    private func prepareViewModel() {
        prepareInput()
        prepareOutput()
        downloadAvatar()
    }
    
    private func prepareInput() {
        input.controller.viewDidLoad.subscribe(onNext: {
            
        }).disposed(by: disposeBag)
    }
    
    private func prepareOutput() {
        output.controller.description.onNext(profile.description)
        output.controller.repositoryName.onNext(profile.repositoryName)
        output.controller.username.onNext(profile.username)
    }
}

extension DetailsViewModel {
    private func downloadAvatar() {
        githubNetworkManager.getAvatar(urlString: profile.avatarUrl) { [weak self] (result) in
            switch result {
            case .success(let image):
                self?.output.controller.avatarImage.onNext(image)
            case .error(let error):
                print(error)
            }
        }
    }
}

// MARK: Input/Output
extension DetailsViewModel {
    
    struct Output {
        let controller = ControllerOutput()
        
        struct ControllerOutput {
            let avatarImage = BehaviorSubject<UIImage?>(value: nil)
            let repositoryName = BehaviorSubject<String?>(value: nil)
            let username = BehaviorSubject<String?>(value: nil)
            let description = BehaviorSubject<String?>(value: nil)
        }
    }
    
    struct Input {
        let controller = ControllerInput()
        
        struct ControllerInput {
            let viewDidLoad = PublishSubject<Void>()
        }
    }
}
