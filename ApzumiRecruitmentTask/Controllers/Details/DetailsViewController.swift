//
//  DetailsViewController.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DetailsViewController: ViewController {

    var viewModel: DetailsViewModel!
    
    @IBOutlet fileprivate weak var avatarImageView: UIImageView!
    @IBOutlet fileprivate weak var repositoryNameLabel: UILabel!
    @IBOutlet fileprivate weak var usernameLabel: UILabel!
    @IBOutlet fileprivate weak var descriptionTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareView()
    }
    
    func configure(with viewModel: DetailsViewModel) {
        self.viewModel = viewModel
    }
}

// MARK: Preparation
extension DetailsViewController {
    private func prepareView() {
        prepareRxObservers()
    }
    
    private func prepareRxObservers() {
        viewModel.output.controller.repositoryName.bind(to: repositoryNameLabel.rx.text).disposed(by: disposeBag)
        viewModel.output.controller.username.bind(to: usernameLabel.rx.text).disposed(by: disposeBag)
        viewModel.output.controller.avatarImage.bind(to: avatarImageView.rx.image).disposed(by: disposeBag)
        viewModel.output.controller.description.bind(to: descriptionTextView.rx.text).disposed(by: disposeBag)
    }
}
