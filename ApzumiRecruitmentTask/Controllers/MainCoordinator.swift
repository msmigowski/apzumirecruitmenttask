//
//  MainCoordinator.swift
//  ApzumiRecruitmentTask
//
//  Created by user on 08/10/2018.
//  Copyright © 2018 Mateusz Śmigowski. All rights reserved.
//

import UIKit
import RxSwift

final class MainCoordinator: Coordinator {
    var disposeBag = DisposeBag()
    var presentation: CoordinatorPresentation
    
    required init(with presentation: CoordinatorPresentation) {
        self.presentation = presentation
    }
    
    func start() {
        guard let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? UINavigationController else {
            fatalError("Initial view controller should have UINavigationViewController type")
        }
        guard let mainViewController = navigationController.topViewController as? MainViewController else {
            fatalError("Wrong root view controller in navigation controller")
        }
        let mainViewModel = MainViewModel()
        prepareMainViewModel(mainViewModel, navigationController: navigationController)
        mainViewController.configure(with: mainViewModel)
        
        present(controller: navigationController)
    }
}

// MARK: Prepare DetailViewController
extension MainCoordinator {
    private func prepareMainViewModel(_ viewModel: MainViewModel, navigationController: UINavigationController) {
        viewModel.output.coordinator.presentDetailsViewController.subscribe(onNext: { [weak navigationController] profile in
            let detailsViewModel = DetailsViewModel(with: profile)
            guard let detailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else {
                fatalError("View controller should be type - DetailsViewController")
            }
            detailsViewController.configure(with: detailsViewModel)
            navigationController?.pushViewController(detailsViewController, animated: true)
        }).disposed(by: disposeBag)
    }
}
